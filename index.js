import { Component } from './src/component';
import { createElement, createElement as jsx } from './src/element';
import { render } from './src/render';


export {
    Component,
    createElement,
    render,
    jsx
};

export default {
    Component,
    createElement,
    render,
    jsx
};
