import { Component } from '../src/component';

const NOOP_UPDATER_QUEUE = {
    enqueueForceUpdate: jest.fn(),
    enqueueSetState: jest.fn()
};
const PROPS = { test: 'test' };

test('Component', () => {
    const component = new Component(PROPS);

    expect(component.props).toEqual(PROPS);
    expect(component.context).toBeUndefined();
    expect(component.updater).toBeUndefined();
});

test('Component with updater', () => {
    const component = new Component(PROPS, null, NOOP_UPDATER_QUEUE);

    expect(component.props).toEqual(PROPS);
    expect(component.context).toBeNull();
    expect(component.updater).toEqual(NOOP_UPDATER_QUEUE);
});

test('setState', () => {
    const component = new Component(PROPS, null, NOOP_UPDATER_QUEUE);
    component.setState();

    expect(component.props).toEqual(PROPS);
    expect(component.context).toBeNull();
    expect(component.updater).toEqual(NOOP_UPDATER_QUEUE);
    expect(component.updater).toEqual(NOOP_UPDATER_QUEUE);
    expect(NOOP_UPDATER_QUEUE.enqueueSetState.mock.calls.length).toBe(1);
});

test('forceUpdate', () => {
    const component = new Component(PROPS, null, NOOP_UPDATER_QUEUE);
    component.forceUpdate();

    expect(component.props).toEqual(PROPS);
    expect(component.context).toBeNull();
    expect(component.updater).toEqual(NOOP_UPDATER_QUEUE);
    expect(component.updater).toEqual(NOOP_UPDATER_QUEUE);
    expect(NOOP_UPDATER_QUEUE.enqueueForceUpdate.mock.calls.length).toBe(1);
});
