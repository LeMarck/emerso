import { createElement } from '../src/element';

/** @jsx createElement */

test('createElement', () => {
    const textElement = createElement('h1', null, 'Text');
    const numberElement = createElement('h1', null, 0);
    const Component = () => <h1>Text</h1>;

    expect(<h1>Text</h1>).toEqual({ attributes: null, children: ['Text'], nodeName: 'h1' });
    expect(textElement).toEqual({ attributes: null, children: ['Text'], nodeName: 'h1' });
    expect(numberElement).toEqual({ attributes: null, children: ['0'], nodeName: 'h1' });
    expect(<Component />).toEqual({ attributes: null, children: null, nodeName: Component });
});
