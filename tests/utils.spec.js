import { isFunction, isTextNode } from '../src/utils';


test('isFunction', () => {
    const fuc = () => 'fuc';
    const variable = 'variable';

    expect(isFunction(fuc)).toBeTruthy();
    expect(isFunction(variable)).toBeFalsy();
});

test('isTextNode', () => {
    const string = 'string';
    const number = 1;
    const children = [];

    expect(isTextNode(string)).toBeTruthy();
    expect(isTextNode(number)).toBeTruthy();
    expect(isTextNode(children)).toBeFalsy();
});
