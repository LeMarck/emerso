import {
    isEventName, getContext,
    filterAttributes, addStyle,
    addListener, removeListeners,
    setAttributes
} from '../src/node';
import { Component } from '../src/component';


test('isEventName', () => {
    expect(isEventName('onClick')).toBeTruthy();
    expect(isEventName('style')).toBeFalsy();
});

test('getContext', () => {
    const component = new Component();
    component.getChildContext = () => ({ test: 'test' });

    expect(getContext(null)).toEqual({});
    expect(getContext(new Component())).toEqual({});
    expect(getContext(new Component({}, { test: 'test' }))).toEqual({ test: 'test' });
    expect(getContext(component)).toEqual({ test: 'test' });
});

test('filterAttributes', () => {
    const attributes = { class: 'test' };
    const pureNode = document.createElement('div');
    const nodeWithAttributes = document.createElement('div');

    nodeWithAttributes.setAttribute('class', 'test');
    nodeWithAttributes.setAttribute('id', 'test');
    nodeWithAttributes.setAttribute('style', 'font-size: 2rem;');

    expect(filterAttributes(pureNode, attributes)).toEqual(attributes);
    expect(filterAttributes(nodeWithAttributes, attributes)).toEqual({});
    expect(nodeWithAttributes.getAttribute('id')).toBeNull();
});

test('addStyle', () => {
    const style = { color: '#fff', fontSize: '2em', 'text-align': 'center' };
    const node = addStyle(document.createElement('div'), style);

    expect(node.__style).toEqual(style);
    expect(node.getAttribute('style'))
        .toBe('color: rgb(255, 255, 255); font-size: 2em; text-align: center;');

    expect(() => addStyle(node, { displey: { test: 'test' } })).toThrow();
});

test('addListener', () => {
    const handleEvent = jest.fn();
    let node = addListener(
        addListener(document.createElement('div'), 'ref', handleEvent, null),
        'onClick', handleEvent, null);

    expect(node.__listeners.length).toBe(1);
    expect(node.__listeners[0].event).toBe('click');
    expect(handleEvent.mock.calls.length).toBe(1);
});

test('removeListeners', () => {
    const node = addListener(document.createElement('div'), 'onClick', jest.fn(), null);

    removeListeners(node);

    expect(node.__listeners.length).toBe(0);
});

test('setAttributes', () => {
    const attributes = {
        value: 'test',
        className: 'test',
        id: 'test',
        style: { fontSize: '2em' },
        onClick: () => jest.fn()
    };
    const node = document.createElement('div');
    setAttributes(node, attributes);

    expect(node.getAttribute('class')).toBe('test');
    expect(node.getAttribute('id')).toBe('test');
    expect(node.value).toBe('test');
    expect(node.__listeners.length).toBe(1);
    expect(node.__style).not.toBeUndefined();
});
