import { createElement } from '../src/element';
import { Component } from '../src/component';
import { render } from '../src/render';

/** @jsx createElement */

class App extends Component {
    render() {
        return <h1>AppComponent</h1>;
    }
}

const Text = () => <h1>BaseComponent</h1>;

test('render', () => {
    const node = document.createElement('div');
    render(<App/>, node);

    expect(node.innerHTML).toBe('<h1>AppComponent</h1>');
});

test('render base component', () => {
    const node = document.createElement('div');
    render(<Text/>, node);

    expect(node.innerHTML).toBe('<h1>BaseComponent</h1>');
});

test('render with callback', () => {
    const callback = jest.fn();

    const node = document.createElement('div');
    render(<Text/>, node, callback);

    expect(node.innerHTML).toBe('<h1>BaseComponent</h1>');
    expect(callback.mock.calls.length).toBe(1);
});
