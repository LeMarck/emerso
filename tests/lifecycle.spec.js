import { init, mount, update, unmount } from '../src/lifecycle';
import { Component } from '../src/component';

const EVENTS = [
    'componentWillMount',
    'componentDidMount',
    'componentWillReceiveProps',
    'shouldComponentUpdate',
    'componentWillUpdate',
    'componentDidUpdate',
    'componentWillUnmount'
];

test('init', () => {
    const component = new Component();

    EVENTS.forEach(event => {
        expect(component[event]).toBeFalsy();
    });

    init(component);

    EVENTS.forEach(event => {
        expect(component[event]).toBeTruthy();
    });
});

test('mount', () => {
    const component = new Component();
    component.componentWillMount = jest.fn();
    component.render = jest.fn();
    component.componentDidMount = jest.fn();

    mount(component);

    expect(component.componentWillMount.mock.calls.length).toBe(1);
    expect(component.render.mock.calls.length).toBe(1);
    expect(component.componentDidMount.mock.calls.length).toBe(1);
});

test('update', () => {
    const component = new Component();
    component.componentWillReceiveProps = jest.fn();
    component.shouldComponentUpdate = jest.fn();
    component.componentWillUpdate = jest.fn();
    component.render = jest.fn();
    component.componentDidUpdate = jest.fn();

    update(component);

    expect(component.componentWillReceiveProps.mock.calls.length).toBe(1);
    expect(component.shouldComponentUpdate.mock.calls.length).toBe(1);
    expect(component.componentWillUpdate.mock.calls.length).toBe(1);
    expect(component.render.mock.calls.length).toBe(1);
    expect(component.componentDidUpdate.mock.calls.length).toBe(1);
});

test('update with force', () => {
    const component = new Component();
    component.componentWillReceiveProps = jest.fn();
    component.shouldComponentUpdate = jest.fn();
    component.componentWillUpdate = jest.fn();
    component.render = jest.fn();
    component.componentDidUpdate = jest.fn();

    update(component, true);

    expect(component.componentWillReceiveProps.mock.calls.length).toBe(1);
    expect(component.shouldComponentUpdate.mock.calls.length).toBe(0);
    expect(component.componentWillUpdate.mock.calls.length).toBe(1);
    expect(component.render.mock.calls.length).toBe(1);
    expect(component.componentDidUpdate.mock.calls.length).toBe(1);
});

test('unmount', () => {
    const component = new Component();
    component.componentWillUnmount = jest.fn();

    unmount(component);

    expect(component.componentWillUnmount.mock.calls.length).toBe(1);
});
