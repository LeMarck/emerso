import queue from '../src/queue';
import { Component } from '../src/component';

class App extends Component {
    state = { count: 1 };
}

test('enqueueForceUpdate', () => {
    queue.enqueueExecute = jest.fn();
    const app = new App();

    queue.enqueueForceUpdate(app);

    expect(app.state).toEqual({ count: 1 });
    expect(queue.enqueueExecute.mock.calls.length).toBe(1);
    expect(queue.enqueueExecute.mock.calls[0]).toEqual([app, true]);
});

test('enqueueForceUpdate with callback', () => {
    queue.enqueueExecute = jest.fn();
    const callback = jest.fn();
    const app = new App();

    queue.enqueueForceUpdate(app, callback);

    expect(app.state).toEqual({ count: 1 });
    expect(app._renderCallbacks.length).toBe(1);
    expect(callback.mock.calls.length).toBe(0);
    expect(queue.enqueueExecute.mock.calls.length).toBe(1);
    expect(queue.enqueueExecute.mock.calls[0]).toEqual([app, true]);
});

test('enqueueSetState', async () => {
    queue.enqueueExecute = jest.fn();
    const app = new App();

    jest.useFakeTimers();
    queue.enqueueSetState(app, { count: 3 });
    jest.runAllTimers();

    expect(app.state).toEqual({ count: 3 });
    expect(queue.enqueueExecute.mock.calls.length).toBe(1);
    expect(queue.enqueueExecute.mock.calls[0]).toEqual([app]);
});

test('enqueueSetState with function', async () => {
    queue.enqueueExecute = jest.fn();
    const app = new App();

    jest.useFakeTimers();
    queue.enqueueSetState(app, prevState => ({ count: prevState.count + 3 }));
    jest.runAllTimers();

    expect(app.state).toEqual({ count: 4 });
    expect(queue.enqueueExecute.mock.calls.length).toBe(1);
    expect(queue.enqueueExecute.mock.calls[0]).toEqual([app]);
});

test('enqueueSetState with callback', async () => {
    queue.enqueueExecute = jest.fn();
    const callback = jest.fn();
    const app = new App();

    jest.useFakeTimers();
    queue.enqueueSetState(app, { count: 3 }, callback);
    jest.runAllTimers();

    expect(app.state).toEqual({ count: 3 });
    expect(app._renderCallbacks.length).toBe(1);
    expect(callback.mock.calls.length).toBe(0);
    expect(queue.enqueueExecute.mock.calls.length).toBe(1);
    expect(queue.enqueueExecute.mock.calls[0]).toEqual([app]);
});
