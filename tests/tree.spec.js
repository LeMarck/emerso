import { createElement } from '../src/element';
import { Component } from '../src/component';
import { createComponent, destroyComponent, renderTree, updateTree } from '../src/tree';
import queue from '../src/queue';

/** @jsx createElement */

class AppComponent extends Component {
    state = { count: 1 };
    context = { test: 'test' };
    render({ children }) {
        return <span>{ children }</span>;
    }
}

const ZeroComponent = () => <span>0</span>;
const EmptyComponent = () => <span/>;

test('createComponent', () => {
    const parent = createComponent(<AppComponent text={ 'text' } />);
    const child = createComponent(<ZeroComponent />, parent);

    expect(parent.updater).toBeTruthy();
    expect(parent.props).toEqual({ text: 'text', children: null });
    expect(child.updater).toBeTruthy();
    expect(child.render).toBeTruthy();
    expect(child.props).toEqual({ children: null });
    expect(child.context).toEqual(parent.context);
});

test('renderTree', () => {
    const text = renderTree(null, 'test');
    const simpleComponent = renderTree(null, <ZeroComponent />);
    const componentWithChild = renderTree(null, <AppComponent><ZeroComponent /></AppComponent>);
    const emptyComponent = renderTree(null, <EmptyComponent />);

    expect(text instanceof Node).toBeTruthy();
    expect(simpleComponent.outerHTML).toBe('<span>0</span>');
    expect(componentWithChild.outerHTML).toBe('<span><span>0</span></span>');
    expect(emptyComponent.outerHTML).toBe('<span></span>');
    expect(simpleComponent.__component).toBeTruthy();
    expect(simpleComponent.__vnode).toBeTruthy();
    expect(componentWithChild.__component).toBeTruthy();
    expect(componentWithChild.__vnode).toBeTruthy();
    expect(emptyComponent.__component).toBeTruthy();
    expect(emptyComponent.__vnode).toBeTruthy();
});

test('destroyComponent', () => {
    const vnode = <AppComponent><ZeroComponent /></AppComponent>;
    const node = renderTree(null, <AppComponent><ZeroComponent /></AppComponent>);
    const parent = node.__component;
    const child = node.children[0].__component;
    parent.componentWillUnmount = jest.fn();
    child.componentWillUnmount = jest.fn();

    const res = destroyComponent(node, vnode);

    expect(parent.componentWillUnmount.mock.calls.length).toBe(1);
    expect(child.componentWillUnmount.mock.calls.length).toBe(1);
    expect(res).toBe(node);
});

test('updateTree', () => {
    class App extends Component {
        state = { text: 'Hello, World!', show: true };
        render() {
            return <div>
                { this.state.show && <span>{ this.state.text }</span> }
                <ZeroComponent />
                { !this.state.show && <span>{ this.state.text }</span> }
                <EmptyComponent />
            </div>;
        }
    }

    const nodeOne = renderTree(null, <App />);
    const nodeTwo = renderTree(null, <App />);
    const nodeThree = renderTree(null, <App />);
    const component = nodeOne.__component;
    const cleanComponent = nodeThree.__component;

    component.state.text = 'New text';
    updateTree(component, nodeOne, component.render());

    component.state.show = false;
    updateTree(component, nodeTwo, component.render());

    queue.enqueueExecute(cleanComponent);

    expect(nodeOne.outerHTML).toBe('<div><span>New text</span><span>0</span><span></span></div>');
    expect(nodeTwo.outerHTML).toBe('<div><span>0</span><span>New text</span><span></span></div>');
    expect(cleanComponent._node.outerHTML)
        .toBe('<div><span>Hello, World!</span><span>0</span><span></span></div>');
});
