const PUNCTUATION = [
    '<', '>', '[', ']', '{', '}', '(', ')',
    ';', '.', ':', '/', '\'', '=', ',', '!'
];
const FUNCTIONS = [
    'render', 'componentDidMount',
    'setInterval', 'setState',
    'componentWillUnmount', 'clearInterval',
    'toLocaleTimeString', 'handleChange',
    'handleSubmit', 'preventDefault',
    'concat', 'map', 'now'
];

export default function colorize(code) {
    const tokens = [];
    let line = '';
    code.split('')
        .forEach(symbol => { // eslint-disable-line
            if (symbol.toLowerCase() !== symbol.toUpperCase() || Number(symbol) || symbol === '0') {
                line += symbol;

                return;
            }
            if (symbol === '&') {
                tokens.push(line);
                line = symbol;

                return;
            }
            if (symbol === ';' && (line === '&lt' || line === '&gt')) {
                tokens.push(line + symbol);
                line = '';

                return;
            }

            if (line.length) {
                tokens.push(line);
                line = '';
            }
            tokens.push(symbol);
        });

    let newCode = '';
    let buffer = '';
    let isAttrValue = false;
    let isClass = false;
    let isTag = false;
    let isTagName = false;

    tokens.forEach(value => { // eslint-disable-line
        if (value === ' ' || value === '\n') {
            newCode += value;

            return;
        }
        if (value === '"' && buffer.length && isAttrValue) {
            newCode += `<span style="color: #8dc891;">${buffer}</span>`;
            buffer = '';
            isAttrValue = false;

            return;
        }
        if (buffer.length && isAttrValue) {
            buffer += value;

            return;
        }
        if (value === '"') {
            buffer = value;
            isAttrValue = true;

            return;
        }
        if (['class', 'extends', 'var', 'let', 'const', 'return', 'new'].includes(value)) {
            newCode += `<span style="color: #c678dd;">${value}</span>`;
            isClass = value === 'class' || value === 'extends';

            return;
        }
        if (PUNCTUATION.includes(value)) {
            newCode += `<span style="color: #5FB3B3;">${value}</span>`;
            if (value === '<') {
                isTag = true;
            }
            if (value === '>') {
                isTag = false;
                isTagName = false;
            }

            return;
        }
        if (FUNCTIONS.includes(value) || Number(value)) {
            newCode += `<span style="color: #79b6f2;">${value}</span>`;

            return;
        }
        if (isTag && !isTagName) {
            newCode += `<span style="color: #e06c75;">${value}</span>`;
            isTagName = true;

            return;
        }
        if (isTagName) {
            newCode += `<span style="color: #c5a5c5;">${value}</span>`;

            return;
        }
        if (['this', 'target'].includes(value)) {
            newCode += `<span style="color: #c5a5c5;">${value}</span>`;

            return;
        }

        if (isClass) {
            newCode += `<span style="color: #FAC863;">${value}</span>`;
            isClass = false;

            return;
        }
        newCode += value;
    });

    return newCode;
}


