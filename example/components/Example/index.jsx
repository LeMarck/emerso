/* eslint-disable brace-style */

import React, { Component } from 'emerso';
import colorize from './code-style';
import './style.styl';

export class Example extends Component {
    render({ title, description, code, instance, attrs }) {
        return <section class="example">
            <header class="example__header">
                <h2 class="example__title">{ title }</h2>
            </header>
            <article class="example__description">{ description }</article>
            <article class="example__demo">
                <pre class="example__code" ref={ pre => { pre.innerHTML = colorize(code); } } />
                <div class="example__result">
                    { React.createElement(instance, attrs) }
                </div>
            </article>
        </section>;
    }
}
