import React, { Component } from 'emerso';
import './style.styl';
import logo from './logo.png';

export class Header extends Component {
    state = {
        title: 'Emerso',
        description: 'A JavaScript library for building user interfaces'
    };
    render(_, { title, description }) {
        return <header class="header">
            <img class="header__logo" src={ logo } align="Emerso" />
            <section>
                <h1 class="header__title">{ title }</h1>
                <h3 class="header__subtitle">{ description }</h3>
            </section>
        </header>;
    }
}
