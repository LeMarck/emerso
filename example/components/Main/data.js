import { Todo } from './apps/todo';
import { Hello } from './apps/hello';
import { Clock } from './apps/clock';

export default [
    {
        title: 'A Simple Component',
        description: `Emerso components implement a render() 
                method that takes input data and returns what to display. 
                This example uses an XML-like syntax called JSX. Input data 
                that is passed into the component can be accessed by render() 
                via this.props.`,
        instance: Hello,
        attrs: { name: 'Evgeny' },
        code: `class Hello extends Component {
    render(props) {
        return <span>Hello, { props.name }</span>;
    }
}

render(<Hello name="Evgeny" />, mountNode);`
    },
    {
        title: 'A Stateful Component',
        description: `In addition to taking input data (accessed via this.props), 
                a component can maintain internal state data (accessed via this.state). 
                When a component’s state data changes, the rendered markup will be updated 
                by re-invoking render().`,
        instance: Clock,
        code: `class Clock extends Component {
    state = { time: Date.now() };

    componentDidMount() {
        this.timer = setInterval(() => {
            this.setState({ time: Date.now() });
        }, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    render(props, state) {
        let time = new Date(state.time)
            .toLocaleTimeString();

        return <span>{ time }</span>;
    }
}


render(<Clock />, mountNode);`
    },
    {
        title: 'An Application',
        description: `Using props and state, we can put together a small Todo 
                application. This example uses state to track the current list of items 
                as well as the text that the user has entered. Although event handlers 
                appear to be rendered inline, they will be collected and implemented 
                using event delegation.`,
        instance: Todo,
        code: `const TodoList = ({ items }) => <ul>
    { items.map(({ text }) => <li>{ text }</li>) }
</ul>;

class Todo extends Component {
    state = { items: [], text: '' };

    handleChange = event => {
        this.setState({ text: event.target.value });
    };

    handleSubmit = event => {
        event.preventDefault();

        const { text } = this.state;

        if (!text.length) return;

        this.setState(prevState => ({
            items: prevState.items.concat({ text }),
            text: ''
        }));
    };

    render(_, { items, text }) {
        return <div>
            <h3>TODO</h3>
            <form onSubmit={ this.handleSubmit }>
                <input
                    onInput={ this.handleChange }
                    value={ text } />
                <button>Add</button>
                <TodoList items={ items } />
            </form>
        </div>;
    }
}

render(<Todo />, mountNode);`
    }
];
