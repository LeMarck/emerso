import React, { Component } from 'emerso';
import { Example } from '../Example';
import data from './data';
import './style.styl';

export class Main extends Component {
    render() {
        return <main class="main">
            { data.map(item => <Example {...item} />)}
        </main>;
    }
}
