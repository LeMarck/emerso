import React, { Component } from 'emerso';

export class Clock extends Component {
    state = { time: Date.now() };
    style = {
        fontSize: '3rem',
        color: 'rgb(55, 121, 188)'
    };

    componentDidMount() {
        this.timer = setInterval(() => {
            this.setState({ time: Date.now() });
        }, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    render(props, state) {
        let time = new Date(state.time)
            .toLocaleTimeString();

        return <span style={ this.style }>{ time }</span>;
    }
}
