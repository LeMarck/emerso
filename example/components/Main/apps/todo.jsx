import React, { Component } from 'emerso';

const TodoList = ({ items }) => <ul>
    { items.map(({ text }) => <li>{ text }</li>) }
</ul>;

export class Todo extends Component {
    state = { items: [], text: '' };

    handleChange = event => {
        this.setState({ text: event.target.value });
    };

    handleSubmit = event => {
        event.preventDefault();

        const { text } = this.state;

        if (!text.length) {
            return;
        }

        this.setState(prevState => ({
            items: prevState.items.concat({ text }),
            text: ''
        }));
    };

    render(_, { items, text }) {
        return <div>
            <h3>TODO</h3>
            <form onSubmit={ this.handleSubmit }>
                <input onInput={ this.handleChange } value={ text } />
                <button>Add</button>
                <TodoList items={ items } />
            </form>
        </div>;
    }
}
