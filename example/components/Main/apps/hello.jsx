import React, { Component } from 'emerso';

export class Hello extends Component {
    render(props) {
        return <span style={{ fontWeight: 'bold' }}>Hello, { props.name }</span>;
    }
}
