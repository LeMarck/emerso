import React, { Component } from 'emerso';
import './style.styl';

export class Footer extends Component {
    state = {
        author: 'Evgeny Petrov',
        year: 2018
    };
    render(_, { author, year }) {
        return <footer class="footer">
            <p class="footer__copyright">
                Copyright © { year } <span class="footer__author">{ author }</span>
            </p>
        </footer>;
    }
}
