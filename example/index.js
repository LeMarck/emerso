import React, { Component, render } from 'emerso';
import './style.styl';

import { Header } from './components/Header';
import { Main } from './components/Main';
import { Footer } from './components/Footer';

class App extends Component {
    render() {
        return <div>
            <Header />
            <Main />
            <Footer />
        </div>;
    }
}

render(<App />, document.getElementById('app'));
