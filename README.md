# Emerso

[![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)
[![Coverage Status](https://coveralls.io/repos/bitbucket/LeMarck/emerso/badge.svg?branch=master)](https://coveralls.io/bitbucket/LeMarck/emerso?branch=master)

A JavaScript library for building user interfaces.

Simple **React** alternative with the same modern API. **Components** & **Virtual DOM**.

## LICENSE

[**MIT**](LICENSE)
