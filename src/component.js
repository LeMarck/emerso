export class Component {

    constructor(props, context, updater) {
        this.props = props;
        this.state = {};
        this.context = context;
        this.updater = updater;
    }

    setState(partialState, callback) {
        this.updater.enqueueSetState(this, partialState, callback);
    }

    forceUpdate(callback) {
        this.updater.enqueueForceUpdate(this, callback);
    }
}
