function noop() { } // eslint-disable-line

export function init(component) {
    [
        'componentWillMount',
        'componentDidMount',
        'componentWillReceiveProps',
        'shouldComponentUpdate',
        'componentWillUpdate',
        'componentDidUpdate',
        'componentWillUnmount'
    ].forEach(event => {
        component[event] = component[event] || noop;
    });

    return component;
}

export function mount(component) {
    init(component);

    component.componentWillMount();

    const vnode = component.render(component.props, component.state, component.context);

    component.componentDidMount();

    return vnode;
}

export function update(component, force) {
    const { props, state, context } = component;
    const prevProps = component.prevProps || props;
    const prevState = component.prevState || state;

    component.componentWillReceiveProps(props, context);
    if (!force) {
        component.shouldComponentUpdate(props, state, context);
    }
    component.componentWillUpdate(props, state, context);

    const vnode = component.render(props, state, context);
    (component._renderCallbacks || []).forEach(cb => cb());

    component.componentDidUpdate(prevProps, prevState);

    return vnode;
}

export function unmount(component) {
    component.componentWillUnmount();
}
