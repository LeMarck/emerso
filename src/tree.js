import { getContext, setAttributes } from './node';
import { mount, update, unmount } from './lifecycle';
import { isFunction, isTextNode } from './utils';
import { Component } from './component';
import updater from './queue';


export function createComponent(component, parentComponent) {
    const { attributes, children } = component;
    const constructor = component.nodeName;
    const props = Object.assign({}, constructor.defaultProps, attributes, { children });
    let context = getContext(parentComponent);

    if (constructor.prototype && constructor.prototype.render) {
        return Object.assign(new constructor(props, context), { updater, parentComponent });
    }

    return Object.assign(new Component(props, context, updater), { render: constructor });
}

export function destroyComponent(node, children) {
    if (isFunction(children.nodeName)) {
        unmount(node.__component);
    }

    (children.children || [])
        .forEach((child, index) =>
            destroyComponent(node.children[index], child));

    return node;
}

export function renderTree(parentComponent, children) {
    if (isTextNode(children)) {
        return document.createTextNode(children);
    }

    while (!children.nodeName.split) {
        parentComponent = createComponent(children, parentComponent);
        children = mount(parentComponent);
    }

    const node = setAttributes(
        document.createElement(children.nodeName),
        children.attributes, parentComponent
    );

    (children.children || [])
        .forEach(child => child && node.appendChild(renderTree(parentComponent, child)));

    node.__component = parentComponent;
    node.__vnode = children;
    parentComponent._node = node;

    return node;
}

// eslint-disable-next-line
export function updateTree(component, node, vnode) {
    const newChildren = (vnode.children || []).slice();
    const oldChildren = (node.__vnode.children || []).slice();
    const nodeChildren = node.children;
    const maxLength = Math.max(newChildren.length, oldChildren.length);
    let position = 0;

    node = setAttributes(node, vnode.attributes, component);

    for (let index = 0; index < maxLength; index++) {

        if (!newChildren[index] && oldChildren[index]) {
            node.removeChild(destroyComponent(nodeChildren[position], oldChildren[index]));
        }

        if (newChildren[index] && !oldChildren[index]) {
            node.insertBefore(
                renderTree(Object.assign({}, component), newChildren[index]),
                nodeChildren[position++]
            );
        }

        if (newChildren[index] && oldChildren[index]) {
            // eslint-disable-next-line max-depth
            if (isTextNode(oldChildren[index]) && oldChildren[index] !== newChildren[index]) {
                node.innerHTML = newChildren[index];
            }

            // eslint-disable-next-line max-depth
            if (nodeChildren[position]) {
                // eslint-disable-next-line max-depth
                if (isFunction(newChildren[index].nodeName)) {
                    const { attributes, children } = newChildren[index];
                    nodeChildren[position].__component.context = getContext(component);
                    component = nodeChildren[position].__component;

                    Object.assign(component.props, attributes, { children });
                    newChildren[index] = update(component);
                    oldChildren[index] = nodeChildren[position].__vnode;
                }

                updateTree(component, nodeChildren[position], newChildren[index]);
                nodeChildren[position].__vnode = newChildren[index];
            }
            position++;
        }
        node.__vnode = vnode;
    }
}
