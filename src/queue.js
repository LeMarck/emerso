import { isFunction } from './utils';
import { update } from './lifecycle';
import { updateTree } from './tree';


export default {
    enqueueExecute: function (publicInstance, force) {
        updateTree(publicInstance, publicInstance._node, update(publicInstance, force));
    },

    enqueueForceUpdate: function (publicInstance, callback) {
        if (callback) {
            (publicInstance._renderCallbacks = (publicInstance._renderCallbacks || []))
                .push(callback);
        }

        this.enqueueExecute(publicInstance, true);
    },

    enqueueSetState: function (publicInstance, partialState, callback) {
        publicInstance.prevProps = Object.assign({}, publicInstance.props);
        publicInstance.prevState = Object.assign({}, publicInstance.state);

        Object.assign(
            publicInstance.state,
            isFunction(partialState) ?
                partialState(publicInstance.state, publicInstance.props) :
                partialState
        );

        if (callback) {
            (publicInstance._renderCallbacks = (publicInstance._renderCallbacks || []))
                .push(callback);
        }

        setTimeout(this.enqueueExecute, 0, publicInstance);
    }
};
