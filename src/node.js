import { isFunction, isTextNode } from './utils';


export const isEventName = name => /^on./.test(name) && !/^on[^A-Z]/.test(name);

export function getContext(parentComponent) {
    parentComponent = parentComponent || {};
    const { context } = parentComponent;
    const childContext = parentComponent.getChildContext && parentComponent.getChildContext();

    return Object.assign({}, context, childContext);
}

export function filterAttributes(node, attributes) {
    attributes = attributes || {};

    [...node.attributes].forEach(attribute => {
        const { name } = attribute;
        const isNodeAttr = node.getAttribute(name) === attributes[name] ||
            (name === 'style' && node.__style === attributes[name]);

        if (isNodeAttr) {
            delete attributes[attribute.name];
        } else {
            node.removeAttribute(attribute.name);
        }
    });

    return attributes;
}

export function addStyle(node, style) {
    Object.keys(style).forEach(key => {
        const value = style[key];

        if (isTextNode(value)) {
            node.style[key.replace(/([A-Z])/g, '-$1').toLowerCase()] = `${value}`;
        } else {
            throw new Error(
                `Expected "string" or "number" but received "${typeof value}"`
            );
        }
    });

    node.__style = style;

    return node;
}

export function addListener(node, eventName, handleEvent, context) {
    if (isEventName(eventName)) {
        const event = eventName.substr(2).toLowerCase();
        const listener = handleEvent.bind(context);

        node.addEventListener(event, listener);
        (node.__listeners = (node.__listeners || [])).push({ event, listener });
    }
    if (eventName === 'ref') {
        handleEvent(node);
    }

    return node;
}

export function removeListeners(node) {
    (node.__listeners || []).forEach(({ event, listener }) => {
        node.removeEventListener(event, listener);
    });

    node.__listeners = [];
}

export function setAttributes(node, attributes, context) {
    removeListeners(node);

    Object.keys(filterAttributes(node, attributes))
        .forEach(attribute => {
            const attributeValue = attributes[attribute];

            if (attribute === 'className') {
                attribute = 'class';
            }

            if (isFunction(attributeValue)) {
                addListener(node, attribute, attributeValue, context);
                attribute = null;
            }

            if (attribute === 'style') {
                addStyle(node, attributeValue);
                attribute = null;
            }

            if (attribute === 'value') {
                node.value = attributeValue;
                attribute = null;
            }

            if (attribute) {
                node.setAttribute(attribute, attributeValue);
            }
        });

    return node;
}
