import { renderTree } from './tree';


export function render(element, container, callback) {
    container.appendChild(renderTree(null, element));
    if (callback) {
        return callback();
    }
}
