export const isFunction = func => func instanceof Function;

export const isTextNode = children =>
    typeof children === 'string' || typeof children === 'number';
