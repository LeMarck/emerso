export function createElement(nodeName, attributes, ...args) {
    let children = args.length ?
        [].concat(...args).map(item => typeof item === 'number' ? `${item}` : item) :
        null;

    return { nodeName, attributes, children };
}
